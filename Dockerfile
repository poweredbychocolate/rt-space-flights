FROM openjdk:8-jdk-alpine
MAINTAINER brelak.dawid
COPY target/space-flights-0.0.1-SNAPSHOT.jar engine.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "engine.jar"]
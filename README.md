#Space flight project
-Spring Boot
-Angular (web-pages)

#How to run  
mvn clean install
java -jar target/space-flights-0.0.1-SNAPSHOT.jar
cd web-pages
npm install
ng serve

#How to insert exaple data
Open => http://localhost:8080/naughty/console/ 
Set JDBC URL => to jdbc:h2:~/space-flight/naughty-database
enter user name => sa
paste content of data-example.sql to top textarea and click run 
Done

# How to build and run spring boot docker image
mvn clean install
docker build -t space-flights/engine .
docker run --name engine_app -it -d -p 8080:8080 space-flights/engine



package space.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * Tourist entity implements {@link EntityId} contains tourist data and optionally his flight list.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-15
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Tourist implements EntityId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @ManyToOne(targetEntity = GenderType.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "genderTypeFK")
    private GenderType gender;
    @Column(nullable = false)
    private String country;
    @Lob
    private String notes;
    @Column(nullable = false)
    private LocalDate birthDate;
    @ManyToMany(fetch = FetchType.EAGER, targetEntity = Flight.class,
            cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "tourist_flight",
            joinColumns = @JoinColumn(name = "tourist_id"),
            inverseJoinColumns = @JoinColumn(name = "flight_id"))
    @JsonIgnoreProperties("touristList")
    private List<Flight> flightList;

    @Override
    public String toString() {
        return "Tourist{" +
                "Id=" + Id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", country='" + country + '\'' +
                ", notes='" + notes + '\'' +
                ", birthDate=" + birthDate +
                ", flights=" + flightList.size() +
                '}';
    }
}

package space.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Flight entity implements {@link EntityId} contains flight data and optionally tourist list.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-15
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@EqualsAndHashCode
public class Flight implements EntityId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private LocalDateTime departureTime;
    @Column(nullable = false)
    private LocalDateTime arrivalTime;
    @Column(nullable = false)
    private Integer seatsNumber;
    @ManyToMany(targetEntity = Tourist.class, fetch = FetchType.EAGER,
            cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JsonIgnoreProperties("flightList")
    @JoinTable(name = "tourist_flight",
            joinColumns = @JoinColumn(name = "flight_id"),
            inverseJoinColumns = @JoinColumn(name = "tourist_id"))
    private List<Tourist> touristList;
    @Column(nullable = false)
    private Double ticketPrice;

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                ", seatsNumber=" + seatsNumber +
                ", tourists=" + touristList.size() +
                ", ticketPrice=" + ticketPrice +
                '}';
    }
}

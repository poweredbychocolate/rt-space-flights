package space.model;

/**
 * The interface Entity id entities base.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-14
 */
public interface EntityId {
    /**
     * Gets entity id.
     *
     * @return the id
     */
    default public Long getId() {
        return -1L;
    }

    /**
     * Sets entity id.
     *
     * @param id the id
     */
    public void setId(Long id);

}

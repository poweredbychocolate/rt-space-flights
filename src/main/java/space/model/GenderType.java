package space.model;

import lombok.*;

import javax.persistence.*;

/**
 * GenderType entity implements {@link EntityId}.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class GenderType implements EntityId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
}

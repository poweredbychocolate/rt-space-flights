package space.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import space.model.Flight;

import java.time.LocalDateTime;
import java.util.List;

/**
 * {@link Flight} repository.
 *
 * @version 2
 * @since 2020 -02-15
 */
public interface FlightRepository extends JpaRepository<Flight, Long> {
    /**
     * Find all by departure time after startDate and departure time before endDate.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @return the list
     */
    List<Flight> findAllByDepartureTimeAfterAndDepartureTimeBefore(LocalDateTime startTime, LocalDateTime endTime);

    /**
     * Find all with pagination by departure time after startDate and departure time before endDate.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param pageable  the pageable
     * @return the page
     */
    Page<Flight> findAllByDepartureTimeAfterAndDepartureTimeBefore(LocalDateTime startTime, LocalDateTime endTime, Pageable pageable);

    /**
     * Find all by ticket price between priceFrom and priceTo.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @return the list
     */
    List<Flight> findAllByTicketPriceBetween(Double priceFrom, Double priceTo);


    /**
     * Find all with pagination by ticket price between priceFrom and priceTo.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @param pageable  the pageable
     * @return the page
     */
    Page<Flight> findAllByTicketPriceBetween(Double priceFrom, Double priceTo, Pageable pageable);


    /**
     * Find all by ticket price between priceFrom and priceTo and departure time after startDate and departure time before endDate.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @param startTime the start time
     * @param endTime   the end time
     * @return the list
     */
    List<Flight> findAllByTicketPriceBetweenAndDepartureTimeAfterAndDepartureTimeBefore(Double priceFrom, Double priceTo, LocalDateTime startTime, LocalDateTime endTime);


    /**
     * Find all with pagination by ticket price between priceFrom and priceTo and departure time after startDate and departure time before endDate.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @param startTime the start time
     * @param endTime   the end time
     * @param pageable  the pageable
     * @return the page
     */
    Page<Flight> findAllByTicketPriceBetweenAndDepartureTimeAfterAndDepartureTimeBefore(Double priceFrom, Double priceTo, LocalDateTime startTime, LocalDateTime endTime, Pageable pageable);
}

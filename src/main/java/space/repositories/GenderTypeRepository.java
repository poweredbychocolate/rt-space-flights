package space.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import space.model.GenderType;

/**
 * {@link GenderType} repository.
 *
 * @version 1
 * @since 2020-02-15
 */
@Repository
public interface GenderTypeRepository extends JpaRepository<GenderType, Long> {
}

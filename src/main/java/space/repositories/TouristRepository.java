package space.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import space.model.Tourist;

import java.util.Optional;

/**
 * {@link Tourist} {@link JpaRepository}.
 *
 * @version 1
 * @since 2020-02-15
 */
@Repository
public interface TouristRepository extends JpaRepository<Tourist, Long> {
    Optional<Tourist> findByFirstNameAndLastName(String firstName,String lastName);
}

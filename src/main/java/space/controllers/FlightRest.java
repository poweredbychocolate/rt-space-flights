package space.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import space.model.EntityId;
import space.model.Flight;
import space.model.Tourist;
import space.repositories.FlightRepository;
import space.repositories.TouristRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for {@link Flight} entity
 *
 * @author Dawid
 * @version 2
 * @since 2020 -02-15
 */
@RestController
@RequestMapping("flight")
@CrossOrigin
public class FlightRest extends GodlikeController<Flight> {
    @Autowired
    private FlightRepository repository;
    @Autowired
    private TouristRepository touristRepository;

    /**
     * Instantiates a new Flight rest.
     */
    public FlightRest() {
    }

    @Override
    JpaRepository<Flight, Long> getRepository() {
        return repository;
    }

    /**
     * Add tourist to flight tourists list
     *
     * @param touristId the tourist id
     * @param flightId  the flight id
     * @return the {@link Flight} entity if tourist added successfully, code 400 if incorrect request, code 401 if Flight with given id not found, code 404 if {@link Tourist} with given id not exists or code 409 if selected {@link Flight#getSeatsNumber()} is not greater than zero.
     */
    @GetMapping("/tourist/{flightId}/{touristId}")
    ResponseEntity<Flight> addTourist(@PathVariable("touristId") Long touristId, @PathVariable("flightId") Long flightId) {
        try {
            Optional<Flight> flight = repository.findById(flightId);
            if (flight.isPresent()) {
                Optional<Tourist> tourist = touristRepository.findById(touristId);
                if (tourist.isPresent()) {
                    if (flight.get().getSeatsNumber() > 0) {
                        flight.get().setSeatsNumber(flight.get().getSeatsNumber() - 1);
                        flight.get().getTouristList().add(tourist.get());
                        return new ResponseEntity<>(repository.save(flight.get()), HttpStatus.OK);
                    } else {
                        return CONFLICT;
                    }
                } else {
                    return NOT_FOUND;
                }
            } else {
                return UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return BAD_REQUEST;
        }
    }

    /**
     * Remove tourist from flight tourists list
     *
     * @param touristId the tourist id
     * @param flightId  the flight id
     * @return the {@link Flight} entity if tourist removed successfully, code 400 if incorrect request, code 401 if Tourist with given id not found or code 404 if {@link Tourist} with given id not exists.
     */
    @DeleteMapping("/tourist/{flightId}/{touristId}")
    ResponseEntity<Flight> removeTourist(@PathVariable("touristId") Long touristId, @PathVariable("flightId") Long flightId) {
        try {
            Optional<Flight> flight = repository.findById(flightId);
            if (flight.isPresent()) {
                Optional<Tourist> tourist = flight.get().getTouristList().stream().filter(tourist1 -> tourist1.getId().equals(touristId)).findFirst();
                if (tourist.isPresent()) {
                    flight.get().setSeatsNumber(flight.get().getSeatsNumber() + 1);
                    flight.get().getTouristList().remove(tourist.get());
                    return new ResponseEntity<>(repository.save(flight.get()), HttpStatus.OK);
                } else {
                    return NOT_FOUND;
                }
            } else {
                return UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return BAD_REQUEST;
        }
    }

    /**
     * Gets {@link Flight} with {@link Flight#getTicketPrice()} between priceFrom and priceTo.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @return the Flight entries with given prices
     */
    @GetMapping("price/{from}/{to}")
    ResponseEntity<List<Flight>> getAll(@PathVariable("from") Double priceFrom, @PathVariable("to") Double priceTo) {
        List<Flight> list = repository.findAllByTicketPriceBetween(priceFrom, priceTo);
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    /**
     * Gets {@link Flight} with {@link Flight#getTicketPrice()} between priceFrom and priceTo.
     * Support pagination, sets header page-count param as total available page count
     * and entity-count as total element count.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @param page      the page
     * @param size      the size
     * @return the Flight entries with given prices
     */
    @GetMapping("price/{from}/{to}/{page}/{size}")
    ResponseEntity<List<Flight>> getAll(@PathVariable("from") Double priceFrom, @PathVariable("to") Double priceTo,
                                        @PathVariable("page") int page, @PathVariable("size") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Flight> flightPage = repository.findAllByTicketPriceBetween(priceFrom, priceTo, pageable);
        List<Flight> list = flightPage.getContent();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("page-count", String.valueOf(flightPage.getTotalPages()));
        httpHeaders.add("entity-count", String.valueOf(flightPage.getTotalElements()));
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    /**
     * Gets all {@link Flight} with {@link Flight#getDepartureTime()} between fromDate and toDate.
     *
     * @param fromDate the from date
     * @param toDate   the to date
     * @return the Flight entries with given DepartureTime
     */
    @GetMapping("date/{from}/{to}")
    ResponseEntity<List<Flight>> getAll(@PathVariable("from") String fromDate, @PathVariable("to") String toDate) {
        LocalTime lt = LocalTime.of(0, 0);
        LocalDateTime f = LocalDateTime.of(LocalDate.parse(fromDate), lt);
        LocalDateTime t = LocalDateTime.of(LocalDate.parse(toDate).plusDays(1), lt);
        List<Flight> list = repository.findAllByDepartureTimeAfterAndDepartureTimeBefore(f, t);
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    /**
     * Gets all {@link Flight} with {@link Flight#getDepartureTime()} between fromDate and toDate.
     * Support pagination, sets header page-count param as total available page count
     * and entity-count as total element count.
     *
     * @param fromDate the from date
     * @param toDate   the to date
     * @param page     the page
     * @param size     the size
     * @return the Flight entries with given DepartureTime
     */
    @GetMapping("date/{from}/{to}/{page}/{size}")
    ResponseEntity<List<Flight>> getAll(@PathVariable("from") String fromDate, @PathVariable("to") String toDate,
                                        @PathVariable("page") int page, @PathVariable("size") int size) {
        LocalTime lt = LocalTime.of(0, 0);
        LocalDateTime f = LocalDateTime.of(LocalDate.parse(fromDate), lt);
        LocalDateTime t = LocalDateTime.of(LocalDate.parse(toDate).plusDays(1), lt);
        Pageable pageable = PageRequest.of(page, size);
        Page<Flight> flightPage = repository.findAllByDepartureTimeAfterAndDepartureTimeBefore(f, t, pageable);
        List<Flight> list = flightPage.getContent();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("page-count", String.valueOf(flightPage.getTotalPages()));
        httpHeaders.add("entity-count", String.valueOf(flightPage.getTotalElements()));
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    /**
     * Gets all {@link Flight} with {@link Flight#getTicketPrice()} between priceFrom and priceTo
     * and {@link Flight#getDepartureTime()} between fromDate and toDate.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @param fromDate  the from date
     * @param toDate    the to date
     * @return the Flight entries with given TicketPrice and DepartureTime
     */
    @GetMapping("price/date/{priceFrom}/{priceTo}/{dateFrom}/{dateTo}")
    ResponseEntity<List<Flight>> getAll(@PathVariable("priceFrom") Double priceFrom, @PathVariable("priceTo") Double priceTo,
                                        @PathVariable("dateFrom") String fromDate, @PathVariable("dateTo") String toDate) {
        LocalTime lt = LocalTime.of(0, 0);
        LocalDateTime f = LocalDateTime.of(LocalDate.parse(fromDate), lt);
        LocalDateTime t = LocalDateTime.of(LocalDate.parse(toDate).plusDays(1), lt);

        List<Flight> list = repository.findAllByTicketPriceBetweenAndDepartureTimeAfterAndDepartureTimeBefore(priceFrom, priceTo, f, t);
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    /**
     * Gets all {@link Flight} with {@link Flight#getTicketPrice()} between priceFrom and priceTo
     * and {@link Flight#getDepartureTime()} between fromDate and toDate.
     * Support pagination, sets header page-count param as total available page count
     * and entity-count as total element count.
     *
     * @param priceFrom the price from
     * @param priceTo   the price to
     * @param fromDate  the from date
     * @param toDate    the to date
     * @param page      the page
     * @param size      the size
     * @return the Flight entries with given TicketPrice and DepartureTime
     */
    @GetMapping("price/date/{priceFrom}/{priceTo}/{dateFrom}/{dateTo}/{page}/{size}")
    ResponseEntity<List<Flight>> getAll(@PathVariable("priceFrom") Double priceFrom, @PathVariable("priceTo") Double priceTo,
                                        @PathVariable("dateFrom") String fromDate, @PathVariable("dateTo") String toDate,
                                        @PathVariable("page") int page, @PathVariable("size") int size) {
        LocalTime lt = LocalTime.of(0, 0);
        LocalDateTime f = LocalDateTime.of(LocalDate.parse(fromDate), lt);
        LocalDateTime t = LocalDateTime.of(LocalDate.parse(toDate).plusDays(1), lt);
        Pageable pageable = PageRequest.of(page, size);
        Page<Flight> flightPage = repository.findAllByTicketPriceBetweenAndDepartureTimeAfterAndDepartureTimeBefore(priceFrom, priceTo, f, t, pageable);
        List<Flight> list = flightPage.getContent();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("page-count", String.valueOf(flightPage.getTotalPages()));
        httpHeaders.add("entity-count", String.valueOf(flightPage.getTotalElements()));
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    /**
     * Gets list of all {@link EntityId}.
     * Support pagination, sets header page-count param as total available page count
     * and entity-count as total element count.
     *
     * @param page the page
     * @param size the size
     * @return the found entity list or code 404 if list is empty
     */
    @GetMapping("/{page}/{size}")
    ResponseEntity<List<Flight>> getAll(@PathVariable("page") int page, @PathVariable("size") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Flight> flightPage = repository.findAll(pageable);
        List<Flight> list = flightPage.getContent();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("page-count", String.valueOf(flightPage.getTotalPages()));
        httpHeaders.add("entity-count", String.valueOf(flightPage.getTotalElements()));
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    //TODO remove if client stop missing header param
    @GetMapping("/counter/{page}/{size}")
    int getPageCount(@PathVariable("page") int page, @PathVariable("size") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(pageable).getTotalPages();
    }

    @GetMapping("counter/price/{from}/{to}/{page}/{size}")
    int getPageCount(@PathVariable("from") Double priceFrom, @PathVariable("to") Double priceTo,
                     @PathVariable("page") int page, @PathVariable("size") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAllByTicketPriceBetween(priceFrom, priceTo, pageable).getTotalPages();
    }

    @GetMapping("counter/date/{from}/{to}/{page}/{size}")
    int getPageCount(@PathVariable("from") String fromDate, @PathVariable("to") String toDate,
                     @PathVariable("page") int page, @PathVariable("size") int size) {
        LocalTime lt = LocalTime.of(0, 0);
        LocalDateTime f = LocalDateTime.of(LocalDate.parse(fromDate), lt);
        LocalDateTime t = LocalDateTime.of(LocalDate.parse(toDate).plusDays(1), lt);
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAllByDepartureTimeAfterAndDepartureTimeBefore(f, t, pageable).getTotalPages();
    }

    @GetMapping("counter/price/date/{priceFrom}/{priceTo}/{dateFrom}/{dateTo}/{page}/{size}")
    int getPageCount(@PathVariable("priceFrom") Double priceFrom, @PathVariable("priceTo") Double priceTo,
                     @PathVariable("dateFrom") String fromDate, @PathVariable("dateTo") String toDate,
                     @PathVariable("page") int page, @PathVariable("size") int size) {
        LocalTime lt = LocalTime.of(0, 0);
        LocalDateTime f = LocalDateTime.of(LocalDate.parse(fromDate), lt);
        LocalDateTime t = LocalDateTime.of(LocalDate.parse(toDate).plusDays(1), lt);
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAllByTicketPriceBetweenAndDepartureTimeAfterAndDepartureTimeBefore(priceFrom, priceTo, f, t, pageable).getTotalPages();
    }
}

package space.controllers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import space.model.EntityId;

import java.util.List;
import java.util.Optional;

/**
 * REST controllers base.
 *
 * @param <T> the {@link EntityId} parameter
 * @author Dawid
 * @version 1
 * @since 2020-02-14
 */
public abstract class GodlikeController<T extends EntityId> {

    public GodlikeController() {
    }

    /**
     * Gets {@link EntityId} repository.
     *
     * @return the {@link JpaRepository}repository
     */
    abstract JpaRepository<T, Long> getRepository();

    final ResponseEntity<T> NOT_FOUND = new ResponseEntity<>(HttpStatus.NOT_FOUND);
    final ResponseEntity<List<T>> NOT_FOUND_LIST = new ResponseEntity<>(HttpStatus.NOT_FOUND);
    final ResponseEntity<T> BAD_REQUEST = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    final ResponseEntity<T> CONFLICT = new ResponseEntity<T>(HttpStatus.CONFLICT);
    final ResponseEntity<T> UNAUTHORIZED = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    final ResponseEntity<T> OK = new ResponseEntity<>(HttpStatus.OK);

    /**
     * Gets list of all {@link EntityId}.
     *
     * @return the found entity list or code 404 if list is empty
     */
    @GetMapping
    ResponseEntity<List<T>> getAll() {
        List<T> list = getRepository().findAll();
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        } else {
            return NOT_FOUND_LIST;
        }
    }

    /**
     * Gets one {@link EntityId} with given id.
     *
     * @param id the entity id
     * @return the one entity or code 404 if not found
     */
    @GetMapping("/{id}")
    ResponseEntity<T> getOne(@PathVariable("id") Long id) {
        Optional<T> optional = getRepository().findById(id);
        return optional.map(t -> new ResponseEntity<>(t, HttpStatus.OK)).orElse(NOT_FOUND);
    }

    /**
     * Save {@link EntityId}.
     *
     * @param toSave {@link EntityId} to save
     * @return saved Entity, code 400 if object cannot be save or is incorrect
     * or code 409 if {@link EntityId#getId()} is already used
     */
    @PostMapping
    ResponseEntity<T> save(@RequestBody T toSave) {
        try {
            if (toSave.getId() != null && getRepository().findById(toSave.getId()).isPresent()) {
                return CONFLICT;
            } else {
                return new ResponseEntity<>(getRepository().save(toSave), HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return BAD_REQUEST;
        }
    }

    /**
     * Update {@link EntityId}.
     *
     * @param toSave {@link EntityId} to save
     * @return updated Entity, code 400 if object cannot be save or is incorrect
     * or code 404 if {@link EntityId#getId()} not exists
     */
    @PutMapping
    ResponseEntity<T> update(@RequestBody T toSave) {
        try {
            if (toSave.getId() != null && getRepository().findById(toSave.getId()).isPresent()) {
                return new ResponseEntity<>(getRepository().save(toSave), HttpStatus.OK);
            } else {
                return NOT_FOUND;
            }
        } catch (Exception e) {
            return BAD_REQUEST;
        }
    }

    /**
     * Remove {@link EntityId} with given Id.
     *
     * @param id the Entity id
     * @return code 200 if removed successfully or code 404 if not
     */
    @DeleteMapping("/{id}")
    ResponseEntity<T> remove(@PathVariable("id") Long id) {
        try {
            getRepository().deleteById(id);
            return OK;
        } catch (Exception e) {
            return NOT_FOUND;
        }
    }

}

package space.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import space.model.Flight;
import space.model.Tourist;
import space.repositories.FlightRepository;
import space.repositories.TouristRepository;

import java.util.Optional;

/**
 * REST Controller for {@link Tourist} entity.
 *
 * @author Dawid
 * @version 2
 * @since 2020 -02-15
 */
@RestController
@RequestMapping("/tourist")
@CrossOrigin
public class TouristRest extends GodlikeController<Tourist> {
    private final ResponseEntity<Tourist> FORBIDDEN = new ResponseEntity<>(HttpStatus.FORBIDDEN);
    @Autowired
    private TouristRepository repository;
    @Autowired
    private FlightRepository flightRepository;

    /**
     * Instantiates a new Tourist rest.
     */
    public TouristRest() {
    }

    @Override
    JpaRepository<Tourist, Long> getRepository() {
        return repository;
    }

    /**
     * Add flight to tourist flights list
     *
     * @param touristId the tourist id
     * @param flightId  the flight id
     * @return the {@link Tourist} entity if flight added successfully, code 400 if incorrect request, code 401 if Tourist with given id not found, code 404 if {@link Flight} with given id not exists or code 409 if selected {@link Flight#getSeatsNumber()} is not greater than zero.
     */
    @GetMapping("/flight/{touristId}/{flightId}")
    ResponseEntity<Tourist> addFlight(@PathVariable("touristId") Long touristId, @PathVariable("flightId") Long flightId) {
        try {
            Optional<Tourist> tourist = repository.findById(touristId);
            if (tourist.isPresent()) {
                Optional<Flight> flight = flightRepository.findById(flightId);
                if (flight.isPresent()) {
                    if (flight.get().getSeatsNumber() > 0) {
                        flight.get().setSeatsNumber(flight.get().getSeatsNumber() - 1);
                        tourist.get().getFlightList().add(flight.get());
                        Tourist tmp = repository.save(tourist.get());
                        return new ResponseEntity<>(tmp, HttpStatus.OK);
                    } else {
                        return CONFLICT;
                    }
                } else {
                    return NOT_FOUND;
                }
            } else {
                return UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return BAD_REQUEST;
        }
    }

    /**
     * Remove flight from tourist flights list
     *
     * @param touristId the tourist id
     * @param flightId  the flight id
     * @return the {@link Tourist} entity if flight removed successfully, code 400 if incorrect request, code 401 if Tourist with given id not found or code 404 if {@link Flight} with given id not exists.
     */
    @DeleteMapping("/flight/{touristId}/{flightId}")
    ResponseEntity<Tourist> removeFlight(@PathVariable("touristId") Long touristId, @PathVariable("flightId") Long flightId) {
        try {
            Optional<Tourist> tourist = repository.findById(touristId);
            if (tourist.isPresent()) {
                Optional<Flight> flight = tourist.get().getFlightList().stream().filter(flight1 -> flight1.getId().equals(flightId)).findFirst();
                if (flight.isPresent()) {
                    flight.get().setSeatsNumber(flight.get().getSeatsNumber() + 1);
                    tourist.get().getFlightList().remove(flight.get());
                    return new ResponseEntity<>(repository.save(tourist.get()), HttpStatus.OK);
                } else {
                    return NOT_FOUND;
                }
            } else {
                return UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return BAD_REQUEST;
        }
    }

    /**
     * Remove {@link Tourist} with given Id.
     *
     * @param id the Tourist id
     * @return code 200 if removed successfully, code 400 if request incorrect request,
     * code 404 if tourist with given id not exists or code 403 if tourist flight list is not empty.
     */
    @DeleteMapping("/{id}")
    @Override
    ResponseEntity<Tourist> remove(@PathVariable("id") Long id) {
        try {
            Optional<Tourist> optionalTourist = repository.findById(id);
            if (optionalTourist.isPresent()) {
                if (optionalTourist.get().getFlightList() == null || optionalTourist.get().getFlightList().isEmpty()) {
                    repository.deleteById(id);
                    return OK;
                } else {
                    return FORBIDDEN;
                }
            } else {
                return NOT_FOUND;
            }
        } catch (Exception e) {
            return BAD_REQUEST;
        }
    }

    /**
     * Check if {@link Tourist} with given firstName and lastName exist.
     *
     * @param fName tourist first name
     * @param lName tourist last name
     * @return the Tourist entity if exist or code 404
     */
    @GetMapping("check/{firstName}/{lastName}")
    ResponseEntity<Tourist> check(@PathVariable("firstName") String fName, @PathVariable("lastName") String lName) {
        Optional<Tourist> optional = repository.findByFirstNameAndLastName(fName, lName);
        return optional.map(t -> new ResponseEntity<>(t, HttpStatus.OK)).orElse(NOT_FOUND);
    }
}

package space.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.model.GenderType;
import space.repositories.GenderTypeRepository;

/**
 * REST controller for {@link GenderType} entity.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-14
 */
@RestController
@RequestMapping("/gender/type")
@CrossOrigin
public class GenderTypeRest extends GodlikeController<GenderType> {
    @Autowired
    private GenderTypeRepository repository;

    public GenderTypeRest() {
    }

    @Override
    JpaRepository<GenderType, Long> getRepository() {
        return repository;
    }
}

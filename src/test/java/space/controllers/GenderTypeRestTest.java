package space.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import space.model.GenderType;
import space.repositories.GenderTypeRepository;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@WebMvcTest(GenderTypeRest.class)
class GenderTypeRestTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private GenderTypeRepository repository;
    private Map<Long, GenderType> map;
    private long index;

    @BeforeEach
    void setUp() {
        index = 1;
        map = new HashMap<>();
        map.put(index, new GenderType(index++, "Unknown"));
        map.put(index, new GenderType(index++, "Female"));
        map.put(index, new GenderType(index++, "Male"));
        map.put(index, new GenderType(index++, "Bigender"));
        map.put(index, new GenderType(index++, "Transgender Female"));
        map.put(index, new GenderType(index++, "Transgender Male"));
        //find all
        when(repository.findAll()).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Find All]");
            return new LinkedList<>(map.values());
        });
        //find by id
        when(repository.findById(anyLong())).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Find By Id]");
            long l = invocationOnMock.getArgument(0);
            return Optional.ofNullable(map.getOrDefault(l, null));
        });
        //save
        when(repository.save(any(GenderType.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Save]");
            GenderType genderType = invocationOnMock.getArgument(0);
            if (genderType.getId() == null) genderType.setId(index++);
            map.put(genderType.getId(), genderType);
            return genderType;
        });
        //delete by id
        doAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Delete By Id]");
            long l = invocationOnMock.getArgument(0);
            if(!map.containsKey(l)) throw new Exception();
            map.remove(l);
            return null;
        }).when(repository).deleteById(anyLong());

    }

    @DisplayName("[Test Base controller find all]")
    @Test
    void findALLTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/gender/type"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[4]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[5]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[6]").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Unknown"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name").value("Male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[3].name").value("Bigender"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[4].name").value("Transgender Female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[5].name").value("Transgender Male"));

        map.clear();
        mockMvc.perform(MockMvcRequestBuilders.get("/gender/type"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());

    }

    @DisplayName("[Test Base controller find by id]")
    @Test
    void findByIdTest() throws Exception {
        GenderType g1 = map.get(2L);
        GenderType g2 = map.get(4L);
        mockMvc.perform(MockMvcRequestBuilders.get("/gender/type/{id}", g1.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(g1.getName()));

        mockMvc.perform(MockMvcRequestBuilders.get("/gender/type/{id}", g2.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(g2.getName()));

        mockMvc.perform(MockMvcRequestBuilders.get("/gender/type/{id}", 100))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @DisplayName("[Test Base controller save]")
    @Test
    void saveTest() throws Exception {
        GenderType g1 = new GenderType();
        g1.setName("Gender Fluid");
        GenderType g2 = new GenderType();
        g2.setName("Agender");
        ObjectMapper mapper = new JsonMapper();

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/gender/type/")
                .content(mapper.writeValueAsString(g1)).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(g1.getName()))
                .andReturn();

        g1 = mapper.readValue(mvcResult.getResponse().getContentAsString(), GenderType.class);
        assertTrue(g1.getId() > 0);

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/gender/type/")
                .content(mapper.writeValueAsString(g2)).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(g2.getName()))
                .andReturn();

        g2 = mapper.readValue(mvcResult.getResponse().getContentAsString(), GenderType.class);
        assertTrue(g2.getId() > 0);
        assertNotEquals(g1, g2);

        mockMvc.perform(MockMvcRequestBuilders.post("/gender/type/")
                .content(mapper.writeValueAsString(g1)).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict());

        mockMvc.perform(MockMvcRequestBuilders.post("/gender/type/")
                .content(mapper.writeValueAsString(g2)).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict());

        mockMvc.perform(MockMvcRequestBuilders.post("/gender/type/")
                .content("Test and test and another test").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @DisplayName("[Test Base controller update]")
    @Test
    void updateTest() throws Exception {
        GenderType g1 = map.get(2L);
        g1.setName("Gender Fluid");
        GenderType g2 = map.get(4L);
        g2.setName("Agender");
        ObjectMapper mapper = new JsonMapper();

        mockMvc.perform(MockMvcRequestBuilders.put("/gender/type/")
                .content(mapper.writeValueAsString(g1)).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(g1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(g1.getName()));

        mockMvc.perform(MockMvcRequestBuilders.put("/gender/type/")
                .content(mapper.writeValueAsString(g2)).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(g2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(g2.getName()));


        mockMvc.perform(MockMvcRequestBuilders.put("/gender/type/")
                .content(mapper.writeValueAsString(new GenderType(100L, "100L"))).contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        mockMvc.perform(MockMvcRequestBuilders.put("/gender/type/")
                .content("Test and test and another test").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @DisplayName("[Test Base controller delete by id]")
    @Test
    void deleteTest() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.delete("/gender/type/{id}", 1))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.delete("/gender/type/{id}", 6))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.delete("/gender/type/{id}", 666))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.delete("/gender/type/{id}", 6))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

}

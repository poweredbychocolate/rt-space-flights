package space.controllers;

import com.google.common.collect.Lists;
import org.assertj.core.internal.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import space.model.Flight;
import space.model.GenderType;
import space.model.Tourist;
import space.repositories.FlightRepository;
import space.repositories.TouristRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@WebMvcTest(FlightRest.class)
class FlightRestTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private FlightRepository repository;
    @MockBean
    private TouristRepository touristRepository;

    private Map<Long, Tourist> map;
    private Flight flight1, flight2, flight3;
    private GenderType mGT, fGT;

    @BeforeEach
    void setUp() {
        mGT = new GenderType(1L, "Male");
        fGT = new GenderType(2L, "Female");

        flight1 = new Flight();
        flight1.setId(1L);
        flight1.setSeatsNumber(30);
        flight1.setTicketPrice(1000.00);
        flight1.setTouristList(new LinkedList<>());
        flight1.setArrivalTime(LocalDateTime.now().plusDays(1).minusHours(3));
        flight1.setDepartureTime(LocalDateTime.now().plusHours(2));

        flight2 = new Flight();
        flight2.setId(2L);
        flight2.setSeatsNumber(10);
        flight2.setTicketPrice(1500.00);
        flight2.setTouristList(new LinkedList<>());
        flight2.setArrivalTime(LocalDateTime.now().plusDays(2).minusHours(10));
        flight2.setDepartureTime(LocalDateTime.now().plusHours(12));

        flight3 = new Flight();
        flight3.setId(3L);
        flight3.setSeatsNumber(5);
        flight3.setTicketPrice(500.00);
        flight3.setTouristList(new LinkedList<>());
        flight3.setArrivalTime(LocalDateTime.now().plusDays(1).minusHours(10));
        flight3.setDepartureTime(LocalDateTime.now().plusHours(4));

        map = new HashMap<>();
        Tourist tourist = new Tourist();
        tourist.setId(1L);
        tourist.setBirthDate(LocalDate.of(1995, 5, 15));
        tourist.setCountry("Poland");
        tourist.setFirstName("Brajanusz");
        tourist.setLastName("Polaczek");
        tourist.setGender(mGT);
        tourist.setNotes("Brajanusz is as tester from Poland");
        map.put(tourist.getId(), tourist);

        tourist = new Tourist();
        tourist.setId(2L);
        tourist.setBirthDate(LocalDate.of(1984, 10, 6));
        tourist.setCountry("Poland");
        tourist.setFirstName("Jessica");
        tourist.setLastName("Kowalska");
        tourist.setGender(fGT);
        tourist.setNotes("Jessica need two seats");
        map.put(tourist.getId(), tourist);

        tourist = new Tourist();
        tourist.setId(3L);
        tourist.setBirthDate(LocalDate.of(1965, 2, 20));
        tourist.setCountry("Czech Republic");
        tourist.setFirstName("Hynek");
        tourist.setLastName("Chodura");
        tourist.setGender(mGT);
        tourist.setNotes("Hynek smell like beer");
        map.put(tourist.getId(), tourist);

        tourist = new Tourist();
        tourist.setId(4L);
        tourist.setBirthDate(LocalDate.of(1999, 12, 18));
        tourist.setCountry("Slovakia");
        tourist.setFirstName("Alica");
        tourist.setLastName("Ericka");
        tourist.setGender(fGT);
        map.put(tourist.getId(), tourist);


        //find all
        when(repository.findAll()).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Find All]");
            return Lists.newArrayList(flight1, flight2, flight3);
        });
        //find by id
        when(touristRepository.findById(anyLong())).thenAnswer(invocationOnMock -> {
            System.err.println("[Tourist Repository] [Find By Id]");
            long l = invocationOnMock.getArgument(0);
            return Optional.ofNullable(map.getOrDefault(l, null));
        });
        //save
        when(repository.save(any(Flight.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Save]");
            Flight tmp = invocationOnMock.getArgument(0);
            if (tmp.getId().equals(flight1.getId())) flight1 = tmp;
            if (tmp.getId().equals(flight2.getId())) flight2 = tmp;
            if (tmp.getId().equals(flight3.getId())) flight3 = tmp;
            return tmp;
        });
        //find flight by id
        when(repository.findById(anyLong())).thenAnswer(invocationOnMock -> {
            long l = invocationOnMock.getArgument(0);
            System.err.println("[Repository] [Delete By Id]");
            if (l == flight1.getId()) return Optional.of(flight1);
            if (l == flight2.getId()) return Optional.of(flight2);
            if (l == flight3.getId()) return Optional.of(flight3);
            return Optional.empty();
        });

    }

    @DisplayName("[FlightRest controller test add tourist]")
    @Test
    void addTourist() throws Exception {

        int cont = flight2.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.get("/flight/tourist/{flightId}/{touristId}", flight2.getId(), map.get(2L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(flight2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.seatsNumber").value(flight2.getSeatsNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ticketPrice").value(flight2.getTicketPrice()));

        assertEquals(cont - 1, flight2.getSeatsNumber());
        assertEquals(1, flight2.getTouristList().size());
        assertTrue(flight2.getTouristList().contains(map.get(2L)));

        flight3.setSeatsNumber(2);
        cont = flight3.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.get("/flight/tourist/{flightId}/{touristId}", flight3.getId(), map.get(3L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(flight3.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.seatsNumber").value(flight3.getSeatsNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ticketPrice").value(flight3.getTicketPrice()));

        mockMvc.perform(MockMvcRequestBuilders.get("/flight/tourist/{flightId}/{touristId}", flight3.getId(), map.get(1L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(flight3.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.seatsNumber").value(flight3.getSeatsNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ticketPrice").value(flight3.getTicketPrice()));

        assertEquals(cont - 2, flight3.getSeatsNumber());
        assertTrue(flight3.getTouristList().contains(map.get(1L)));
        assertTrue(flight3.getTouristList().contains(map.get(3L)));

        mockMvc.perform(MockMvcRequestBuilders.get("/flight/tourist/{flightId}/{touristId}", flight3.getId(), map.get(2L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.get("/flight/tourist/{flightId}/{touristId}", flight3.getId(), map.get(4L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());


        mockMvc.perform(MockMvcRequestBuilders.get("/flight/tourist/{flightId}/{touristId}", flight2.getId(), 120253))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.get("/flight/tourist/{flightId}/{touristId}", 569, 115023))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @DisplayName("[FlightRest controller test remove tourist]")
    @Test
    void removeTouristTest() throws Exception {
        flight1.getTouristList().add(map.get(1L));
        flight1.getTouristList().add(map.get(3L));
        flight2.getTouristList().add(map.get(1L));
        flight2.getTouristList().add(map.get(2L));
        flight2.getTouristList().add(map.get(3L));
        flight2.getTouristList().add(map.get(4L));

        int cont = flight1.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.delete("/flight/tourist/{flightId}/{touristId}", flight1.getId(), map.get(3L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(flight1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.seatsNumber").value(flight1.getSeatsNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ticketPrice").value(flight1.getTicketPrice()));

        assertEquals(cont + 1, flight1.getSeatsNumber());
        assertEquals(1, flight1.getTouristList().size());
        assertFalse(flight1.getTouristList().contains(map.get(3L)));

        mockMvc.perform(MockMvcRequestBuilders.delete("/flight/tourist/{flightId}/{touristId}", flight1.getId(), map.get(3L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        cont = flight2.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.delete("/flight//tourist/{flightId}/{touristId}", flight2.getId(), map.get(1L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(flight2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.seatsNumber").value(flight2.getSeatsNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ticketPrice").value(flight2.getTicketPrice()));

        mockMvc.perform(MockMvcRequestBuilders.delete("/flight//tourist/{flightId}/{touristId}", flight2.getId(), map.get(4L).getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(flight2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.seatsNumber").value(flight2.getSeatsNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ticketPrice").value(flight2.getTicketPrice()));

        assertEquals(cont + 2, flight2.getSeatsNumber());
        assertEquals(2, flight2.getTouristList().size());
        assertFalse(flight2.getTouristList().contains(map.get(1L)));
        assertFalse(flight2.getTouristList().contains(map.get(4L)));


        mockMvc.perform(MockMvcRequestBuilders.delete("/flight/tourist/{flightId}/{touristId}", flight3.getId(), 12300))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.delete("/flight/tourist/{flightId}/{touristId}", 500, 12300))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }
}
package space.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import space.model.Flight;
import space.model.GenderType;
import space.model.Tourist;
import space.repositories.FlightRepository;
import space.repositories.TouristRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(TouristRest.class)
class TouristRestTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TouristRepository repository;
    @MockBean
    private FlightRepository flightRepository;

    private Map<Long, Tourist> map;
    private Flight flight1, flight2, flight3;
    private GenderType mGT, fGT;

    @BeforeEach
    void setUp() {
        mGT = new GenderType(1L, "Male");
        fGT = new GenderType(2L, "Female");
        flight1 = new Flight();
        flight1.setId(1L);
        flight1.setSeatsNumber(30);
        flight1.setTicketPrice(1000.00);
        flight1.setArrivalTime(LocalDateTime.now().plusDays(1).minusHours(3));
        flight1.setDepartureTime(LocalDateTime.now().plusHours(2));

        flight2 = new Flight();
        flight2.setId(2L);
        flight2.setSeatsNumber(10);
        flight2.setTicketPrice(1500.00);
        flight2.setArrivalTime(LocalDateTime.now().plusDays(2).minusHours(10));
        flight2.setDepartureTime(LocalDateTime.now().plusHours(12));

        flight3 = new Flight();
        flight3.setId(3L);
        flight3.setSeatsNumber(5);
        flight3.setTicketPrice(500.00);
        flight3.setArrivalTime(LocalDateTime.now().plusDays(1).minusHours(10));
        flight3.setDepartureTime(LocalDateTime.now().plusHours(4));

        map = new HashMap<>();
        Tourist tourist = new Tourist();
        tourist.setId(1L);
        tourist.setBirthDate(LocalDate.of(1995, 5, 15));
        tourist.setCountry("Poland");
        tourist.setFirstName("Brajanusz");
        tourist.setLastName("Polaczek");
        tourist.setGender(mGT);
        tourist.setFlightList(new LinkedList<>());
        tourist.setNotes("Brajanusz is as tester from Poland");
        map.put(tourist.getId(), tourist);

        tourist = new Tourist();
        tourist.setId(2L);
        tourist.setBirthDate(LocalDate.of(1984, 10, 6));
        tourist.setCountry("Poland");
        tourist.setFirstName("Jessica");
        tourist.setLastName("Kowalska");
        tourist.setGender(fGT);
        tourist.setFlightList(new LinkedList<>());
        tourist.setNotes("Jessica need two seats");
        map.put(tourist.getId(), tourist);

        tourist = new Tourist();
        tourist.setId(3L);
        tourist.setBirthDate(LocalDate.of(1965, 2, 20));
        tourist.setCountry("Czech Republic");
        tourist.setFirstName("Hynek");
        tourist.setLastName("Chodura");
        tourist.setGender(mGT);
        tourist.setFlightList(new LinkedList<>());
        tourist.setNotes("Hynek smell like beer");
        map.put(tourist.getId(), tourist);

        tourist = new Tourist();
        tourist.setId(4L);
        tourist.setBirthDate(LocalDate.of(1999, 12, 18));
        tourist.setCountry("Slovakia");
        tourist.setFirstName("Alica");
        tourist.setLastName("Ericka");
        tourist.setGender(fGT);
        tourist.setFlightList(new LinkedList<>());
        map.put(tourist.getId(), tourist);


        //find all
        when(repository.findAll()).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Find All]");
            return new LinkedList<>(map.values());
        });
        //find by id
        when(repository.findById(anyLong())).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Find By Id]");
            long l = invocationOnMock.getArgument(0);
            return Optional.ofNullable(map.getOrDefault(l, null));
        });
        //save
        when(repository.save(any(Tourist.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Save]");
            Tourist tmp = invocationOnMock.getArgument(0);
            map.put(tmp.getId(), tmp);
            return tmp;
        });
        //delete by id
        doAnswer(invocationOnMock -> {
            System.err.println("[Repository] [Delete By Id]");
            long l = invocationOnMock.getArgument(0);
            if (!map.containsKey(l)) throw new Exception();
            map.remove(l);
            return null;
        }).when(repository).deleteById(anyLong());
        //find flight by id
        when(flightRepository.findById(anyLong())).thenAnswer(invocationOnMock -> {
            long l = invocationOnMock.getArgument(0);
            System.err.println("[Flight Repository] [Delete By Id]");
            if(l==flight1.getId())return Optional.of(flight1);
            if(l==flight2.getId())return Optional.of(flight2);
            if(l==flight3.getId())return Optional.of(flight3);
            return Optional.empty();
        });

    }

    @DisplayName("[TouristRest controller test add flight]")
    @Test
    void addFlightTest() throws Exception {

        Tourist tourist1 = map.get(2L);
        tourist1.getFlightList().add(flight1);
        tourist1.getFlightList().add(flight3);
        Tourist tourist2 = map.get(4L);
        tourist2.getFlightList().add(flight2);
        tourist2.getFlightList().add(flight3);

        int cont = flight2.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.get("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), flight2.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(tourist1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(tourist1.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(tourist1.getLastName()));

        assertEquals(cont - 1, flight2.getSeatsNumber());
        assertEquals(3, tourist1.getFlightList().size());
        assertTrue(tourist1.getFlightList().contains(flight2));

        flight3.setSeatsNumber(2);
        cont = flight3.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.get("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), flight3.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(tourist1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(tourist1.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(tourist1.getLastName()));

        mockMvc.perform(MockMvcRequestBuilders.get("/tourist/flight/{touristId}/{flightId}", tourist2.getId(), flight3.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(tourist2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(tourist2.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(tourist2.getLastName()));

        assertEquals(cont - 2, flight3.getSeatsNumber());
        assertTrue(tourist1.getFlightList().contains(flight3));
        assertTrue(tourist2.getFlightList().contains(flight3));

        mockMvc.perform(MockMvcRequestBuilders.get("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), flight3.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.get("/tourist/flight/{touristId}/{flightId}", tourist2.getId(), flight3.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());


        mockMvc.perform(MockMvcRequestBuilders.get("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), 123))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.get("/tourist/flight/{touristId}/{flightId}", 50, 123))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @DisplayName("[TouristRest controller test remove flight]")
    @Test
    void removeFlightTest() throws Exception {
        Tourist tourist1 = map.get(2L);
        tourist1.getFlightList().add(flight1);
        tourist1.getFlightList().add(flight3);
        Tourist tourist2 = map.get(4L);
        tourist2.getFlightList().add(flight2);
        tourist2.getFlightList().add(flight3);

        int cont = flight1.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), flight1.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(tourist1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(tourist1.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(tourist1.getLastName()));

        assertEquals(cont + 1, flight1.getSeatsNumber());
        assertEquals(1, tourist1.getFlightList().size());
        assertFalse(tourist1.getFlightList().contains(flight1));

        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), flight1.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        cont = flight3.getSeatsNumber();
        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), flight3.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(tourist1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(tourist1.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(tourist1.getLastName()));

        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/flight/{touristId}/{flightId}", tourist2.getId(), flight3.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(tourist2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(tourist2.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(tourist2.getLastName()));

        assertEquals(cont + 2, flight3.getSeatsNumber());
        assertEquals(0, tourist1.getFlightList().size());
        assertEquals(1, tourist2.getFlightList().size());
        assertFalse(tourist1.getFlightList().contains(flight3));
        assertFalse(tourist2.getFlightList().contains(flight3));


        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/flight/{touristId}/{flightId}", tourist1.getId(), 123))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/flight/{touristId}/{flightId}", 50, 123))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @DisplayName("[TouristRest controller test remove ]")
    @Test
    void removeTest() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/{id}", 1))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/{id}", 1))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        Tourist tourist = map.get(2L);
        tourist.getFlightList().add(flight1);
        tourist.getFlightList().add(flight3);
        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/{id}", tourist.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        tourist.getFlightList().remove(flight1);
        tourist.getFlightList().remove(flight3);
        mockMvc.perform(MockMvcRequestBuilders.delete("/tourist/{id}", tourist.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }
}
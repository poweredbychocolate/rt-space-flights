package space.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Sort;
import space.model.GenderType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class GenderTypeRepositoryTest {
    @Autowired
    private GenderTypeRepository repository;

    @Test
    void baseTest() {
        assertNotNull(repository);
        GenderType g1;
        GenderType g2;
        GenderType g3;
        GenderType g4;
        GenderType g5;
        GenderType g6;

        g1 = assertDoesNotThrow(() -> repository.save(new GenderType(null, "Unknown")));
        g2 = assertDoesNotThrow(() -> repository.save(new GenderType(null, "Female")));
        g3 = assertDoesNotThrow(() -> repository.save(new GenderType(null, "Male")));
        g4 = assertDoesNotThrow(() -> repository.save(new GenderType(null, "Bigender")));
        g5 = assertDoesNotThrow(() -> repository.save(new GenderType(null, "Transgender Female")));
        g6 = assertDoesNotThrow(() -> repository.save(new GenderType(null, "Transgender Male")));

        assertNotNull(g1);
        assertNotNull(g2.getId());
        assertNotEquals(g3.getId(), 0);
        assertNotEquals(g4.getId(), -1L);
        assertTrue(g5.getId() > 0);

        assertEquals(repository.count(), 6);
        assertEquals(g1.getName(), "Unknown");
        assertEquals(g2.getName(), "Female");
        assertEquals(g3.getName(), "Male");
        assertEquals(g4.getName(), "Bigender");
        assertEquals(g5.getName(), "Transgender Female");
        assertEquals(g6.getName(), "Transgender Male");

        assertEquals(g1, repository.findById(g1.getId()).get());
        assertEquals(g2, repository.getOne(g2.getId()));
        assertTrue(repository.findById(g3.getId()).isPresent());

        assertEquals(repository.findAll().size(), 6);
        assertDoesNotThrow(() -> repository.deleteById(g5.getId()));
        assertDoesNotThrow(() -> repository.delete(g6));

        assertFalse(repository.findById(g5.getId()).isPresent());
        assertFalse(repository.findById(g6.getId()).isPresent());

        List<GenderType> list = assertDoesNotThrow(() -> repository.findAll(Sort.by("name")));
        assertEquals(g1, list.get(3));
        assertEquals(g2, list.get(1));
        assertEquals(g3, list.get(2));
        assertEquals(g4, list.get(0));
    }

}
import { Component, OnInit } from '@angular/core';
import { Tourist } from 'src/app/model/tourist';
import { Flight } from 'src/app/model/flight';
import { FlightService } from 'src/app/services/flight.service';
import { TouristService } from 'src/app/services/tourist.service';

@Component({
  selector: 'app-users-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {
  user: Tourist;
  firstName: string;
  lastName: string;
  flights: Flight[];
  priceFrom: number;
  priceTo: number;
  dateFrom: string;
  dateTo: string;
  pageSize = 30;
  pageCount: number;
  menuCounter = 0;
  dateSelector = false;
  priceSelector = false;

  constructor(private flightService: FlightService, private touristService: TouristService) { }

  ngOnInit(): void {
    this.flightService.findAllP(this.menuCounter, this.pageSize).subscribe(result => {
      // console.log(result);
      // console.log(result.status);
      // console.log(result.url);
      // console.log(result.headers);
      // console.log(result.body);
      // console.log(result.headers.get('page-count'));
      this.flights = result.body;
      // this.pageCount = Number(result.headers.get('page-count'));
    });
    this.flightService.findAllPagesCount(this.menuCounter, this.pageSize).subscribe(result => {
      // Naughty fix missing page-count
      this.pageCount = result;
    });
  }
  switchContent(): void {
    if (!this.dateSelector && !this.priceSelector) {
      this.flightService.findAllP(this.menuCounter, this.pageSize).subscribe(result => {
        this.flights = result.body;
      });
    } else if (this.dateSelector && this.priceSelector) {
      this.flightService.findAllByPriceAndDateP(this.priceFrom, this.priceTo,
        this.dateFrom, this.dateTo, this.menuCounter, this.pageSize).subscribe(result => {
          this.flights = result;
        });
    } else if (this.dateSelector && !this.priceSelector) {
      this.flightService.findAllByDateP(this.dateFrom, this.dateTo, this.menuCounter, this.pageSize).subscribe(result => {
        this.flights = result;
      });
    } else if (!this.dateSelector && this.priceSelector) {
      this.flightService.findAllByPriceP(this.priceFrom, this.priceTo, this.menuCounter, this.pageSize).subscribe(result => {
        this.flights = result;
      });
    }
  }

  nextPage(): void {
    if (this.menuCounter < (this.pageCount - 1)) {
      this.menuCounter++;
      this.switchContent();
    }
  }
  previousPage(): void {
    if (this.menuCounter > 0) {
      this.menuCounter--;
      this.switchContent();
    }
  }
  loginUser(formid: string): void {
    this.touristService.check(this.firstName, this.lastName).subscribe(result => {
      this.user = result;
      document.getElementById(formid).style.display = 'none';
      this.firstName = null;
      this.lastName = null;
    });
  }
  logoutUser(): void {
    this.user = null;
  }
  search(): void {
    this.menuCounter = 0;
    if (!this.hasCorrectPrices() && !this.hasCorrectDates()) {
      this.flightService.findAllP(this.menuCounter, this.pageSize).subscribe(result => {
        this.flights = result.body;
        this.dateSelector = false;
        this.priceSelector = false;
      });
      this.flightService.findAllPagesCount(this.menuCounter, this.pageSize).subscribe(result => {
        this.pageCount = result;
      });
    } else if (this.hasCorrectPrices() && this.hasCorrectDates()) {
      this.flightService.findAllByPriceAndDateP(this.priceFrom, this.priceTo,
        this.dateFrom, this.dateTo, this.menuCounter, this.pageSize).subscribe(result => {
          this.flights = result;
          this.dateSelector = true;
          this.priceSelector = true;
        });
      this.flightService.findAllByPriceAndDatePagesCount(this.priceFrom, this.priceTo,
        this.dateFrom, this.dateTo, this.menuCounter, this.pageSize).subscribe(result => {
          this.pageCount = result;
        });
    } else if (!this.hasCorrectPrices() && this.hasCorrectDates()) {
      this.flightService.findAllByDateP(this.dateFrom, this.dateTo, this.menuCounter, this.pageSize).subscribe(result => {
        this.flights = result;
        this.dateSelector = true;
        this.priceSelector = false;
      });
      this.flightService.findAllByDatePagesCount(this.dateFrom, this.dateTo, this.menuCounter, this.pageSize).subscribe(result => {
        this.pageCount = result;
      });
    } else if (this.hasCorrectPrices() && !this.hasCorrectDates()) {
      this.flightService.findAllByPriceP(this.priceFrom, this.priceTo, this.menuCounter, this.pageSize).subscribe(result => {
        this.flights = result;
        this.dateSelector = false;
        this.priceSelector = true;
      });
      this.flightService.findAllByPricePagesCount(this.priceFrom, this.priceTo, this.menuCounter, this.pageSize).subscribe(result => {
        this.pageCount = result;
      });
    }
  }
  hasCorrectDates(): boolean {
    return typeof this.dateFrom === 'string' && typeof this.dateTo === 'string' && this.dateFrom.length > 0 && this.dateTo.length > 0;
  }
  hasCorrectPrices(): boolean {
    return typeof this.priceFrom === 'number' && typeof this.priceTo === 'number' && this.priceFrom > 0 && this.priceTo > 0;
  }

  showDialog(id: string) {
    document.getElementById(id).style.display = 'block';
  }
  hideDialog(id: string) {
    document.getElementById(id).style.display = 'none';
  }
  addFlight(flightId: number, flightIndex: number) {
    this.touristService.addFlight(this.user.id, flightId).subscribe(result => {
      this.user = result;
      for (const f of result.flightList) {
        if (f.id === flightId) {
          this.flights[flightIndex] = f;
        }
      }
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GodlikepanelComponent } from './components/godlikepanel/godlikepanel.component';
import { TouristComponent } from './components/tourist/tourist.component';
import { GenderComponent } from './components/gender/gender.component';
import { FlightComponent } from './components/flight/flight.component';

const godlikeRoutes = [
  {
    path: 'godlikePanel', component: GodlikepanelComponent,
    children: [
      { path: 'tourist', component: TouristComponent },
      { path: 'gender', component: GenderComponent },
      { path: 'flight', component: FlightComponent },
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(godlikeRoutes)
  ],
  exports: [RouterModule]

})
export class GodlikeRouteModule { }

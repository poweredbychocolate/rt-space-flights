import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Flight } from '../model/flight';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { restUrl } from '../app.component';


@Injectable({
  providedIn: 'root'
})
export class FlightService {

  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = restUrl + '/flight';
  }
  public findAll(): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.baseUrl);
  }

  public save(flight: Flight): Observable<Flight> {
    return this.http.post<Flight>(this.baseUrl, flight);
  }
  public update(flight: Flight): Observable<Flight> {
    return this.http.put<Flight>(this.baseUrl, flight);
  }
  public delete(id: number): Observable<any> {
    return this.http.delete<any>(this.baseUrl + '/' + id.toString());
  }
  public addTourist(flightId: number, touristId: number) {
    return this.http.get<Flight>(this.baseUrl + '/tourist/' + flightId + '/' + touristId);
  }
  public removeTourist(flightId: number, touristId: number) {
    return this.http.delete<Flight>(this.baseUrl + '/tourist/' + flightId + '/' + touristId);
  }
  public findAllByPrice(priceFrom: number, priceTo: number): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.baseUrl + '/price/' + priceFrom + '/' + priceTo);
  }
  public findAllByDate(dateFrom: string, dateTo: string): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.baseUrl + '/date/' + dateFrom + '/' + dateTo);
  }
  public findAllByPriceAndDate(priceFrom: number, priceTo: number, dateFrom: string, dateTo: string): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.baseUrl + '/price/date/' + priceFrom + '/' + priceTo + '/' + dateFrom + '/' + dateTo);
  }
  // pagination
  public findAllP(page: number, size: number): Observable<HttpResponse<Flight[]>> {
    return this.http.get<Flight[]>(this.baseUrl + '/' + page + '/' + size, { observe: 'response' });

  } public findAllPagesCount(page: number, size: number): Observable<number> {
    return this.http.get<number>(this.baseUrl + '/counter/' + page + '/' + size);

  }
  public findAllByPriceP(priceFrom: number, priceTo: number, page: number, size: number): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.baseUrl + '/price/' + priceFrom + '/' + priceTo + '/' + page + '/' + size);
  }
  public findAllByPricePagesCount(priceFrom: number, priceTo: number, page: number, size: number): Observable<number> {
    return this.http.get<number>(this.baseUrl + '/counter/price/' + priceFrom + '/' + priceTo + '/' + page + '/' + size);
  }

  public findAllByDateP(dateFrom: string, dateTo: string, page: number, size: number): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.baseUrl + '/date/' + dateFrom + '/' + dateTo + '/' + page + '/' + size);
  }
  public findAllByDatePagesCount(dateFrom: string, dateTo: string, page: number, size: number): Observable<number> {
    return this.http.get<number>(this.baseUrl + '/counter/date/' + dateFrom + '/' + dateTo + '/' + page + '/' + size);
  }
  public findAllByPriceAndDateP(
    priceFrom: number, priceTo: number, dateFrom: string, dateTo: string, page: number, size: number): Observable<Flight[]> {
    return this.http.get<Flight[]>(
      this.baseUrl + '/price/date/' + priceFrom + '/' + priceTo + '/' + dateFrom + '/' + dateTo + '/' + page + '/' + size);
  }
  public findAllByPriceAndDatePagesCount(
    priceFrom: number, priceTo: number, dateFrom: string, dateTo: string, page: number, size: number): Observable<number> {
    return this.http.get<number>(
      this.baseUrl + '/counter/price/date/' + priceFrom + '/' + priceTo + '/' + dateFrom + '/' + dateTo + '/' + page + '/' + size);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GenderType } from 'src/app/model/gendertype';
import { restUrl } from '../app.component';


@Injectable({
  providedIn: 'root'
})
export class GenderService {
  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = restUrl + '/gender/type';
  }
  public findAll(): Observable<GenderType[]> {
    return this.http.get<GenderType[]>(this.baseUrl);
  }
  public save(genderType: GenderType): Observable<GenderType> {
    return this.http.post<GenderType>(this.baseUrl, genderType);
  }
  public delete(id: number): Observable<any> {
    return this.http.delete<any>(this.baseUrl + '/' + id.toString());
  }
}

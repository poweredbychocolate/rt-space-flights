import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tourist } from '../model/tourist';
import { restUrl } from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class TouristService {

  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = restUrl + '/tourist';
  }
  public findAll(): Observable<Tourist[]> {
    return this.http.get<Tourist[]>(this.baseUrl);
  }
  public save(tourist: Tourist): Observable<Tourist> {
    return this.http.post<Tourist>(this.baseUrl, tourist);
  }
  public update(tourist: Tourist): Observable<Tourist> {
    return this.http.put<Tourist>(this.baseUrl, tourist);
  }
  public delete(id: number): Observable<any> {
    return this.http.delete<any>(this.baseUrl + '/' + id.toString());
  }
  public addFlight(touristId: number, flightId: number) {
    return this.http.get<Tourist>(this.baseUrl + '/flight/' + touristId + '/' + flightId);
  }
  public removeFlight(touristId: number, flightId: number) {
    return this.http.delete<Tourist>(this.baseUrl + '/flight/' + touristId + '/' + flightId);
  }
  public check(firstName: string, lastName: string): Observable<Tourist> {
    return this.http.get<Tourist>(this.baseUrl + '/check/' + firstName + '/' + lastName);
  }
}

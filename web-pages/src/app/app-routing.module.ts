import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GodlikepanelComponent } from './components/godlikepanel/godlikepanel.component';
import { FlightComponent } from './userscomponents/flight/flight.component';


const appRoute: Routes = [
  { path: 'godlikePanel', pathMatch: 'full', redirectTo: 'godlikePanel/flight' },
  { path: 'godlikePanel', component: GodlikepanelComponent },
  { path: 'flight', component: FlightComponent },
  { path: '', pathMatch: 'full', redirectTo: '/flight' }
];
@NgModule({
  imports: [RouterModule.forRoot(appRoute)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

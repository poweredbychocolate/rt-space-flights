import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { GodlikepanelComponent } from './components/godlikepanel/godlikepanel.component';
import { GodlikepanelModule } from './godlikepanel.module';
import { FlightComponent } from './userscomponents/flight/flight.component';
import { TouristService } from './services/tourist.service';
import { FlightService } from './services/flight.service';


@NgModule({
  declarations: [
    AppComponent,
    GodlikepanelComponent,
    FlightComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    GodlikepanelModule
  ],
  providers: [TouristService, FlightService],
  bootstrap: [AppComponent]
})
export class AppModule { }

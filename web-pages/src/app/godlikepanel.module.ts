import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightComponent } from './components/flight/flight.component';
import { GenderComponent } from './components/gender/gender.component';
import { TouristComponent } from './components/tourist/tourist.component';
import { GodlikeRouteModule } from './godlike-route.module';
import { GenderService } from './services/gender.service';
import { TouristService } from './services/tourist.service';
import { FlightService } from './services/flight.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [FlightComponent, GenderComponent, TouristComponent],
  imports: [
    CommonModule,
    GodlikeRouteModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [GenderService, TouristService, FlightService]
})
export class GodlikepanelModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodlikepanelComponent } from './godlikepanel.component';

describe('GodlikepanelComponent', () => {
  let component: GodlikepanelComponent;
  let fixture: ComponentFixture<GodlikepanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodlikepanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodlikepanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

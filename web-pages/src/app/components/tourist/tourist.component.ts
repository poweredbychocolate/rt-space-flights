import { Component, OnInit } from '@angular/core';
import { Tourist } from 'src/app/model/tourist';
import { TouristService } from 'src/app/services/tourist.service';
import { GenderType } from 'src/app/model/gendertype';
import { GenderService } from 'src/app/services/gender.service';
import { Flight } from 'src/app/model/flight';
import { FlightService } from 'src/app/services/flight.service';

@Component({
  selector: 'app-tourist',
  templateUrl: './tourist.component.html',
  styleUrls: ['./tourist.component.css']
})
export class TouristComponent implements OnInit {

  tourists: Tourist[];
  tourist: Tourist;
  genders: GenderType[];
  dialogTourist: Tourist;
  dialogNote: string;
  flights: Flight[];
  flight: Flight;
  index: number;


  constructor(private touristService: TouristService, private genderService: GenderService, private flightService: FlightService) {
    this.tourist = new Tourist();
    this.dialogTourist = new Tourist();
  }

  ngOnInit(): void {
    this.touristService.findAll().subscribe(data => {
      this.tourists = data;
    });
    this.genderService.findAll().subscribe(list => {
      this.genders = list;
    });
  }

  save(): void {
    this.touristService.save(this.tourist).subscribe(result => this.tourists.push(result));
    this.tourist = new Tourist();
  }

  delete(id: number) {
    this.touristService.delete(id).subscribe(result => {
      this.ngOnInit();
    });
  }
  removeFlight(flightId: number) {
    this.touristService.removeFlight(this.dialogTourist.id, flightId).subscribe(result => {
      this.tourists[this.index] = result;
      this.dialogTourist = result;
    });
  }
  addFlight() {
    this.touristService.addFlight(this.dialogTourist.id, this.flight.id).subscribe(result => {
      this.tourists[this.index] = result;
      this.dialogTourist = result;
    });
  }

  showDialog(index: number, id: string, dialogTourist: Tourist) {
    document.getElementById(id).style.display = 'block';
    this.dialogTourist = dialogTourist;
    this.index = index;
    this.flightService.findAll().subscribe(result => {
      this.flights = result;
    });
  }

  showNote(id: string, note: string) {
    document.getElementById(id).style.display = 'block';
    this.dialogNote = note;
  }

  hideDialog(id: string) {
    document.getElementById(id).style.display = 'none';
  }

}

import { Component, OnInit } from '@angular/core';
import { Flight } from 'src/app/model/flight';
import { FlightService } from 'src/app/services/flight.service';
import { TouristService } from 'src/app/services/tourist.service';
import { Tourist } from 'src/app/model/tourist';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  flights: Flight[];
  flight: Flight;
  dialogFlight: Flight;
  tourists: Tourist[];
  tourist: Tourist;
 index: number;

  constructor(private flightService: FlightService, private touristService: TouristService) {
    this.flight = new Flight();
    this.dialogFlight = new Flight();
  }

  ngOnInit(): void {
    this.flightService.findAll().subscribe(data => {
      this.flights = data;
    });

  }
  save(): void {
    this.flightService.save(this.flight).subscribe(result => this.flights.push(result));
    this.flight = new Flight();
  }

  delete(id: number) {
    this.flightService.delete(id).subscribe(result => {
      this.ngOnInit();
    });
  }
  removeTourist(touristId: number) {
    this.flightService.removeTourist(this.dialogFlight.id, touristId).subscribe(result => {
      this.flights[this.index] = result;
      this.dialogFlight = result;
    });
  }
  addTourist() {
    this.flightService.addTourist(this.dialogFlight.id, this.tourist.id).subscribe(result => {
      this.flights[this.index] = result;
      this.dialogFlight = result;
    });
  }

  showDialog(index: number, id: string, dialogFlight: Flight) {
    document.getElementById(id).style.display = 'block';
    this.dialogFlight = dialogFlight;
    this.index = index;
    this.touristService.findAll().subscribe(result => {
      this.tourists = result;
    });
  }
  hideDialog(id: string) {
    document.getElementById(id).style.display = 'none';
  }
}

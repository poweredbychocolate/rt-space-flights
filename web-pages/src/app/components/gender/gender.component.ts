import { Component, OnInit } from '@angular/core';
import { GenderType } from 'src/app/model/gendertype';
import { GenderService } from 'src/app/services/gender.service';

@Component({
  selector: 'app-gender-list',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.css']
})
export class GenderComponent implements OnInit {

  genders: GenderType[];
  gender: GenderType;

  constructor(private genderService: GenderService) {
    this.gender = new GenderType();
  }

  ngOnInit(): void {
    this.genderService.findAll().subscribe(data => {
      this.genders = data;
    });
  }

  save(): void {
    this.genderService.save(this.gender).subscribe(result => this.genders.push(result));
    this.gender = new GenderType();
  }
  delete(id: number) {
    this.genderService.delete(id).subscribe(result => {
      this.ngOnInit();
    });
  }

}

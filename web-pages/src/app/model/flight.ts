import { Tourist } from './tourist';

export class Flight {
    id: number;
    departureTime: Date;
    arrivalTime: Date;
    seatsNumber: number;
    ticketPrice: number;
    touristList: Tourist[];
}

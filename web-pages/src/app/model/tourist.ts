import { GenderType } from './gendertype';
import { Flight } from './flight';

export class Tourist {
 id: number;
 firstName: string;
 lastName: string;
 gender: GenderType;
 country: string;
 notes: string;
 flightList: Flight[];
birthDate: Date;
}
